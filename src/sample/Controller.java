package sample;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sample.model.Patient;
import sample.model.Practitioner;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.UnaryOperator;

import static sample.utils.URLs.*;


public class Controller implements Initializable {
    public JFXTextField txtPractitionerID;
    public TableColumn<Patient, String> colPatientName;
    public TableView<Patient> tblAllPatients;
    public TableColumn<Patient, String> colMPatientName;
    public TableColumn<Patient, Double> colMPatientCholesterol;
    public TableColumn<Patient, String> colMTime;
    public TableView<Patient> tblMonitoredPatients;
    public TableColumn<Patient, String> colPatientID;
    public TableColumn<Patient, String> colMPatientID;
    private Practitioner currentUser;
    private String currentID;
    private ArrayList<Patient> myPatientsList = new ArrayList<>();
    private ArrayList<Patient> monitoredPatients = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setNumericOnly();
    }

    private void setNumericOnly() {
        //sets the practitioner textfield to only accept numbers
        UnaryOperator<TextFormatter.Change> filter = t -> {
            if (t.isReplaced())
                if (t.getText().matches("[^0-9]"))
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));

            if (t.isAdded()) {
                if (t.getControlText().contains(".")) {
                    if (t.getText().matches("[^0-9]")) {
                        t.setText("");
                    }
                } else if (t.getText().matches("[^0-9.]")) {
                    t.setText("");
                }
            }
            return t;
        };
        txtPractitionerID.setTextFormatter(new TextFormatter<>(filter));
    }

    public void minimiseApp(MouseEvent event) {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        s.setIconified(true);

    }

    public void maximiseApp(MouseEvent event) {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        s.setMaximized(!s.isMaximized());

    }

    public void closeApp(MouseEvent event) {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        s.close();
    }

    public void searchForPatients(Event event) {
        String practitionerID = txtPractitionerID.getText().trim();
        getPractitioner(practitionerID);

    }

    private void getPractitioner(String practitionerID) {
        try {
            JSONObject json = readJsonFromUrl(practitionerURL + practitionerID + jSONFormat);
            currentID = json.getString("id");
            readEncounters(currentID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readEncounters(String currentID) {
        //22694
        try {
            JSONObject json = readJsonFromUrl(encountersPageURL);
            JSONArray results = json.getJSONArray("entry");
            for (int j = 0; j < results.length(); j++) {
                JSONObject resource = results.getJSONObject(j).getJSONObject("resource");
                JSONArray fields = resource.getJSONArray("participant");
                JSONObject ix = fields.getJSONObject(0).getJSONObject("individual");
                String practitionerID = ix.getString("reference");
                String[] id = practitionerID.split("/");
                String pracID = id[1];
                System.out.println(pracID + "--------" + currentID);
                //   System.out.println(" practitioner ID in read enc " + pracID);
                if (pracID.matches(String.valueOf(currentID))) {
                    JSONObject patientField = resource.getJSONObject("subject");
                    String patientIDFull = patientField.getString("reference");
                    String[] pid = patientIDFull.split("/");
                    String patientID = pid[1];
                    System.out.println("patientID " + patientID);
                    Patient patient = new Patient(patientID);
                    if(myPatientsList.size()==0){
                        myPatientsList.add(patient);
                    } else{
                        for (int i = 0; i < myPatientsList.size(); i++) {
                            if (!myPatientsList.get(i).getId().matches(patientID)) {
                                myPatientsList.add(patient);
                            }
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getPatientData();

    }

    private void getPatientData() {
        System.out.println("items found = " + myPatientsList.size());
        for (Patient patient : myPatientsList) {
            try {
                JSONObject json = readJsonFromUrl(patientsQueryURL + patient.getId() + jSONFormat);
                JSONArray field = json.getJSONArray("name");
                JSONArray name = field.getJSONObject(0).getJSONArray("given");
                String patientName = name.get(0).toString();
                System.out.println("------------------------------------------------------------");
                System.out.println(patientName);
                // System.out.println(json.toString());
                patient.setName(patientName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        populateTable();
    }

    private void populateTable() {
        for (int i = 0; i < myPatientsList.size(); i++) {
            String patientID = myPatientsList.get(i).getId();

        }
        tblAllPatients.setItems(FXCollections.observableArrayList(myPatientsList));
        for (int i = 0; i < myPatientsList.size(); i++) {
            colPatientID.setCellValueFactory(new PropertyValueFactory<>("id"));
            colPatientName.setCellValueFactory(new PropertyValueFactory<>("name"));
        }
        tblAllPatients.refresh();

    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    private void setCurrentPractitioner(Practitioner practitioner) {
        currentUser = practitioner;
    }

    public void monitorPatient(MouseEvent event) {
        Patient patient = tblAllPatients.getSelectionModel().getSelectedItem();
        Practitioner me = new Practitioner(patient);
        me.setId(Integer.parseInt(currentID));
        setCurrentPractitioner(me);
        patient.registerObserver(this.currentUser);
        patient.notifyObservers();
        addToMonitoredTable(patient);
    }

    private void addToMonitoredTable(Patient patient) {
        monitoredPatients.add(patient);
        //  getObservations();

        //schedules it to run once per hour
        Runnable getObservationsRunnable = new Runnable() {
            public void run() {
                getObservations();
            }
        };
        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(getObservationsRunnable, 0, 1, TimeUnit.HOURS);
    }

    private void getObservations() {
        try {
            JSONObject json = readJsonFromUrl(observationPageURL);
            JSONArray results = json.getJSONArray("entry");
            System.out.println("*****************************");
            System.out.println(results.toString());
            for (Patient monitoredPatient : monitoredPatients) {
                for (int j = 0; j < results.length(); j++) {
                    String patientID = results.getJSONObject(j).getJSONObject("resource").getJSONObject("subject").getString("reference");
                    String[] id = patientID.split("/");
                    String patID = id[1];
                    if (patID.matches(String.valueOf(monitoredPatient.getId()))) {
                        String cholesterolValue = results.getJSONObject(j).getJSONObject("resource").getJSONObject("valueQuantity").getString("value");
                        monitoredPatient.setLatestCholesterol(Double.parseDouble(cholesterolValue));
                        String lastUpdated = results.getJSONObject(j).getJSONObject("resource").getJSONObject("meta").getString("lastUpdated");
                        monitoredPatient.setTime(lastUpdated);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        refreshMonitoredPatientsTable();
    }

    private void refreshMonitoredPatientsTable() {
        tblMonitoredPatients.setItems(FXCollections.observableArrayList(monitoredPatients));
        for (int i = 0; i < myPatientsList.size(); i++) {
            colMPatientID.setCellValueFactory(new PropertyValueFactory<>("id"));
            colMPatientName.setCellValueFactory(new PropertyValueFactory<>("name"));
            colMPatientCholesterol.setCellValueFactory(new PropertyValueFactory<>("latestCholesterol"));
            colMTime.setCellValueFactory(new PropertyValueFactory<>("time"));
        }
        tblMonitoredPatients.refresh();
    }

    public void removeObserver(MouseEvent event) {
        Patient patient = tblMonitoredPatients.getSelectionModel().getSelectedItem();
        patient.removeObserver(currentUser);
        monitoredPatients.remove(patient);
        refreshMonitoredPatientsTable();
    }
}
