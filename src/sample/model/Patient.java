package sample.model;

import sample.model.observer.Observer;
import sample.model.observer.Subject;

import java.util.ArrayList;

public class Patient implements Subject {
    private ArrayList<Observer> observers = new ArrayList<>();
    private String name, id;
    private String time;
    private double latestCholesterol;

    public Patient(ArrayList<Observer> observers, String name, String id, String time, double latestCholesterol) {
        this.observers = observers;
        this.name = name;
        this.id = id;
        this.time = time;
        this.latestCholesterol = latestCholesterol;
    }

    public Patient(String id) {
        this.id = id;
    }

    public int hashcode() {
        return id.hashCode();
    }

    public Patient() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getLatestCholesterol() {
        return latestCholesterol;
    }

    public void setLatestCholesterol(double latestCholesterol) {
        this.latestCholesterol = latestCholesterol;
        measurementChanged();
    }

    private void measurementChanged() {
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer observer) {

        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
