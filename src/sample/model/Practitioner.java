package sample.model;

import sample.model.observer.Observer;

public class Practitioner implements Observer {
    private int id;
    private Patient patient;

    public Practitioner(Patient patient) {
        this.patient = patient;
    }

    public Practitioner(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void update() {
        this.patient.getLatestCholesterol();
    }
}
