package sample.model.observer;

public interface Observer {
    void update();
}
