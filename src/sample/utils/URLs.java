package sample.utils;

public abstract class URLs {

    public static final String url = "http://hapi.fhir.org/baseDstu3/";
    public static final String practitionerURL = "http://hapi.fhir.org/baseDstu3/Practitioner/";
    public static final String encountersPageURL = "http://hapi.fhir.org/baseDstu3/Encounter?_pretty=true&_format=json";
    public static final String patientsQueryURL = "http://hapi.fhir.org/baseDstu3/Patient/";
    public static final String observationPageURL = "http://hapi.fhir.org/baseDstu3/Observation/?_format=json";

    public static final String jSONFormat = "/?_format=json";
}
